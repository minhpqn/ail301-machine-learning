# Practice Exercises of the course AIL301 (Machine Learning)

## Chapter 1. Decision Trees

In exercises of this chapter, we will use two data sets:

```tennis.csv``` is the dataset described in the Chapter 3 "Decision Tree Learning" of the book Machine Learning, by Tom Mitchell. This data is in csv format with headers.

The second data set is [Banknote Dataset](http://archive.ics.uci.edu/ml/datasets/banknote+authentication).
The banknote dataset involves predicting whether a given banknote is authentic given a number of measures taken from a photograph.

The dataset contains 1,372 with 5 numeric variables. It is a classification problem with two classes (binary classification).

Below provides a list of the five variables in the dataset.

- variance of Wavelet Transformed image (continuous).
- skewness of Wavelet Transformed image (continuous).
- kurtosis of Wavelet Transformed image (continuous).
- entropy of image (continuous).
- class (integer).

### 00. Calculate Entropy

Write a function ```entropy``` to calculate Entropy for a set of samples. Each
sample is a tuple of attributes and the last attribute is the class label.

Use the written function to calculate the Entropy for all training examples in 
the data set ``tennis`` and ``banknote``.

### 01. Calculate Entropy of a subset

In decision trees, we split training examples into subset corresponding to each categorical attribute.
In this exercise, you will write a function ```entropy_subset``` which have three arguments:

- A set *D* of examples. Each example is a tuples of attributes and the last attribute is the class label.
- Index of the categorical attribute
- Value of the categorical attribute

The function return the entropy of the subset obtained by the value of the categorical attribute.

For example, we will calculate the entropy *E(Outlook=sunny)* for the attribute *Outlook* in the ``tennis`` data set
by calling ```entropy_subset(D, 0, 'sunny')```.

Verify the function by comparing results you calculated by hand on the *tennis* data set.

### 02. Calculate Average Entropy

In this function, we will calculate the average entropy (information) for an entire split by a categorical attribute.
You will need to write a function ```average_entropy``` with two arguments.

- A set ```S``` of tuples of attributes. The last value of a tuple if the class label.
- An index ```i``` of a attribute.

For example, we will calculate average Entropy for the attribute Outlook in the data set ```tennis``` by
calling ```average_entropy(S, 0)```.

Use the function to calculate average Entropy for each attribute (except the class label) for the data set.

### 03. Calculate information gain

Use the functions in the exercises 00~02 to write the function ```info_gain``` to calculate the information gain
for a split by the categorical attribute.

### 04. Calculate split information

In this function, we will write a function ```split_info``` to calcualte Split Information of a data set at the attribute A.

### 05. Calculate Gain Ratio

Use the ```info_gain``` (in the exercise 03) and ```split_info``` to calculate the gain ratio of a data set at the attribute A.

### 06. Calculate Gini Index

Write the function ```gini_index``` to calculate the Gini Index for a data set.

Calculate the Gini Index of the training examples in the data set ```tennis``` and ```banknote```.

### 07. Calculate Average Gini Index

Write the function ```average_gini_index``` to calculate average Gini Index of a split by an attribute *A*.

### 08. Incorporating Continuous-Valued Attributes

Reference: Section 3.7.2 in the textbook "Machine Learning", by Tom Mitchell. You can download the textbook [here](http://www.cs.ubbcluj.ro/~gabis/ml/ml-books/McGrawHill%20-%20Machine%20Learning%20-Tom%20Mitchell.pdf).

In the data set "banknote", attributes are real-values, so to apply ID3 algorithm, we need to convert
continous attributes into discrete values. One way is to find a threshold c for a real-valued attribute A, and create
a new boolean attribute A_c that is true if A < c, and false otherwise. We would like to pick a threshold c, that produces the greatest information gain, gain ratio, or gini index.

We can find the value c by following steps:

- Sort the examples according to the continuous attribute A, then identifying adjacent examples that differ in their target classification, we can generate a set of candidate thresholds midway between the corresponding values of A.
- These candidate thresholds can then be evaluated by computing the information gain associated with each.

Write a function ```calc_threshold(D, i)``` to calculate the threshold for the real-valued attribute A at index ```i```.

Apply the function on the banknote data set.

### 09. Implement Decision Tree algorithm

Follow the tutorial on [How To Implement The Decision Tree Algorithm From Scratch In Python](http://machinelearningmastery.com/implement-decision-tree-algorithm-scratch-python/), implement the ID3 algorithm using three attribute selection methods:

- Information Gain
- Gain Ratio
- Gini Index

Apply the implemented algorithm on two data sets: tennis and banknote.

After implementing decision tree by yourself, use ```DecisionTreeClassifier``` class in the module ```sklearn.tree``` to learn a decision tree model from the data. An then, visualize the learned trees.

## Chapter 2. Linear Regression

In this exercise, you will estimate parameters for linear gression models on the two data sets ```Forbes.txt``` and ```Fuel_Consumption.csv```. The detailed description of the two data sets can be found in the lecture slide about Linear Regression, by Dr. Le Hong Phuong.

### 09. Data Visualization

Visualize the relation between temperature and pressure in the data set ```Forbes.txt``` by using the library ```matplotlib```.

### 10. Normal Equation

Implement Normal Equation method to estimate parameters for linear regression models.

We can learn the implementation from the tutorial [Bài 3: Linear Regression](http://machinelearningcoban.com/2016/12/28/linearregression/), by Vũ Hữu Tiệp.

Estimate parameters of the model on the two data sets ```Forbes.txt``` and ```Fuel_Consumption.csv```.

### 11. Using scikit-learn to estimate parameters of linear regression models

Use ```sklearn.linear_model.LinearRegression``` to fit the linear regression model on the two data set.

Compare the results with your Normal Equation implementation.

### 12. Implement gradient descent algorithm for linear regression

In this exercise, you will implement gradient descent algorithm to estimate parameters of linear regression model.

You can follow the tutorial in [Bài 7: Gradient Descent (phần 1/2)](http://machinelearningcoban.com/2017/01/12/gradientdescent/).

Compare the result returned by gradient descent algorithm with the results in exercise 10 and 11.

### 13. Implement Stochastic Gradient Descent algorithm

Implement Stochastic Gradient Descent algorithm for linear regression and apply on two data sets.

### 14. Implement Mini-batch Gradient Descent algorithm

Implement Mini-batch Gradient Descent algorithm for linear regression and apply on two data sets.

Try with diffent batch sizes and learning rates.

## Chapter 3. Logistic Regression


 