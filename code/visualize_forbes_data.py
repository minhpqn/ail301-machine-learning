"""Visualize Forbes dataset
"""
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model

forbes_data_file = '../datasets/Forbes.txt'
df = pd.read_csv(forbes_data_file, sep=' ', header=None, comment='#')
df.columns=['Temperature', 'Pressure']
plt.plot(df['Temperature'], df['Pressure'], 'o')
plt.xlabel('Temperature (F)')
plt.ylabel('Pressure (inches Hg)')
plt.savefig('fb-1.eps')

# fit the model by Linear Regression
X    = np.array([ df['Temperature'] ]).T
one  = np.ones((X.shape[0], 1))
Xbar = np.concatenate((one, X), axis = 1)
y    = np.array([ df['Pressure'] ]).T
regr = linear_model.LinearRegression(fit_intercept=False) # fit_intercept = False for calculating the bias
regr.fit(Xbar, y)
print( 'Solution found by scikit-learn  : ', regr.coef_ )

# Plot fitting line
w_0 = regr.coef_[0][0]
w_1 = regr.coef_[0][1]
x0 = np.linspace(190, 215, 2)
y0 = w_0 + w_1*x0
plt.plot(x0, y0)
plt.savefig('fb-2.eps')
