\documentclass[hyperref={pdfpagelabels=false},serif]{beamer}
\usepackage{lmodern}
\usepackage{xcolor}
\usepackage{subfigure}
\usetheme{Warsaw}
%\usetheme{Boadilla}
\setbeamertemplate{headline}{}
\usepackage{graphicx}
\usepackage{outlines}
\usepackage{courier}
\usepackage{amssymb, amsmath, amsfonts} 
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{fancyvrb}

\title[Decision Trees  \hspace{25mm} \insertframenumber/\inserttotalframenumber]{Decision Trees}
\author[Pham Quang Nhat Minh]{Pham Quang Nhat Minh}
\institute{FPT Technology Research Institute (FTRI)\\
minhpqn@fpt.edu.vn}
\date{\today} 
\begin{document}
\begin{frame}[plain]
\titlepage
\end{frame} 

% Hightlight current section
\AtBeginSection[]
{
\begin{frame}<beamer>
  \frametitle{Table of Contents}
  \tableofcontents[currentsection]
\end{frame}
}

\begin{frame}
\frametitle{Table of contents}
\tableofcontents
\end{frame}

\begin{frame}
\frametitle{Learning objectives}
\begin{outline}
\1 Explain decision-tree induction algorithms
\1 Implement a decision tree classifier
\1 Try decision-tree algorithms in some off-the-self machine learning toolkits
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Readings}
\begin{outline}
\1 Chapter 3 ``Decision Tree learning``, \textit{Machine Learning}, by Tom M. Mitchell
\1 Section 6.3 ``Classification by Decision Tree Induction``, \textit{Data Mining: Concepts and Techniques}, by Han, Jiawei and Kamber, Micheline.
\end{outline}
\end{frame}

\section{Introduction}

\begin{frame}
\frametitle{Decision Trees for $PlayTennis$}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/dt-f1.ps}
\end{center}
\end{figure}
\textit{The picture is credited by Tom Mitchell, 1997}
\end{frame}

\begin{frame}
\frametitle{Decision Trees}
\begin{outline}
\1 A decision tree consists of
\2 \textbf{Internal nodes}:
\3 test for the value of a certain attribute
\2 \textbf{Branch}:
\3 correspond to the outcome of a test
\3 connect to the next node or leaf
\2 \textbf{Leaf Nodes}:
\3 terminal nodes that predict the outcome
\1 To classify an example
\2[1.] start at the root
\2 [2.] perform the test
\2[3.] follow the branch corresponding to outcome
\2[4.] goto 2. unless leaf
\2[5.] predict that outcome associated with the leaf
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Decision Trees}
In general, decision trees represent a disjunction of conjunctions of constraints on the attribute values of instances
\begin{align*}
& (Outlook=Sunny \land \text{ } Humidity=Normal)\\
\lor & \text{ } (Outlook=Overcast) & \\
\lor & \text{ } (Outlook=Rain \land Wind=Weak) & \\
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Why Decision Trees?}
\begin{outline}
\1 Appropriate for exploratory knowledge discovery
\1 Decision trees can handle high dimensional data
\1 Their representation of acquired knowledge in tree form is intuitive and generally easy to assimilate by humans.
\1 The learning and classification steps of decision tree induction are simple and fast
\1 In general, decision tree classifiers have good accuracy.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{When to Consider Decision Trees}
\begin{outline}
\1 Instances describable by attribute--value pairs
\1 Target function is discrete valued
\1 Disjunctive hypothesis may be required
\1 Possibly noisy training data
\end{outline}

\bigskip
Examples:
\begin{outline}
\1 Equipment or medical diagnosis
\1 Credit risk analysis
\1 Modeling calendar scheduling preferences 
\end{outline}
\end{frame}

% https://tex.stackexchange.com/questions/86101/beamer-creating-a-slide-with-short-centered-prominent-text
\begin{frame}[plain,c]
%\frametitle{A first slide}
\begin{center}
\huge How do we induce a decision tree from a training data?
\end{center}
\end{frame}

\section{Decision Tree Induction}

\begin{frame}
\frametitle{A sample training data}
\begin{table}
\begin{small}
\begin{tabular}{c c c c c c c}
\hline
Day & Outlook & Temperature & Humidity & Wind & PlayTensis\\
\hline
D1 & Sunny & Hot & High & Weak & No \\
D2 & Sunny & Hot & High & Strong & No \\
D3 & Overcast & Hot & High & Weak & Yes \\
D4 & Rain & Mild & High & Weak & Yes \\
D5 & Rain & Cool & Normal & Weak & Yes \\
D6 & Rain & Cool & Normal & Strong & No \\
D7 & Overcast & Cool & Normal & Strong & Yes \\
D8 & Sunny & Mild & High & Weak & No \\
D9 & Sunny & Cool & Normal & Weak & Yes \\
D10 & Rain & Mild & Normal & Weak & Yes \\
D11 & Sunny & Mild & Normal & Strong & Yes \\
D12 & Overcast & Mild & High & Strong & Yes \\
D13 & Overcast & Hot & Normal & Weak & Yes \\
D14 & Rain & Mild & High & Strong & No \\  \hline
\end{tabular}
\end{small}
\caption{Training examples for the target concept $PlayTensis$}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Decision Tree Induction}
\begin{outline}
\1 Builds a set of IF-THEN rules, which can be visualized as a tree, for testing class membership of data points.
\1 Many decision tree (induction) algorithms have been proposed 
\2 ID3 (Iterative Dichotomiser) \& C4.5 (Quinlan)
\2 CART (Classification And Regression Trees)
\2 SLIQ \& SPRINT
\2 Rainforest \& BOAT
\1 ID3, C4.5, and CART adopt a greedy (i.e., nonbacktracking) approach in which decision trees are constructed in a top-down recursive divide-and- conquer manner.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Top-Down Induction of Decision Trees}
Main loop:
\begin{outline}
\1 $A \leftarrow$ the ``best'' decision attribute for next $node$
\1 Assign $A$ as decision attribute for $node$
\1 For each value of $A$, create new descendant of $node$
\1 Sort training examples to leaf nodes
\1 If training examples perfectly classified, Then STOP, Else iterate over
new leaf nodes
\end{outline}
\end{frame}

\begin{frame}
\frametitle{ID3 Algorithm}
Function ID3
\begin{outline}
\1 Input: Example set $S$
\1 Output: Decision Tree $DT$
\end{outline}
Algorithm
\begin{outline}
\1 If all examples in $S$ belong to the same class $c$
\2 return a new leaf and label it with $c$
\1 Else
\2[i.] Select an attribute $A$ according to some heuristic function
\2[ii.] Generate a new node $DT$ with $A$ as test
\2[iii.] For each Value $v_i$ of $A$
\3[(a)] Let $S_i$ = all examples in $S$ with $A = v_i$
\3[(b)] Use ID3 to construct a decision tree $DT_i$ for example set $S_i$
\3[(c)] Generate a branch that connects $DT$ and $DT_i$
\end{outline}
(\textit{For more details, see readings.})
\end{frame}

\begin{frame}
\frametitle{Attribute Selection Problem}
Which attribute is best? 

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/dt-s1.ps}
\end{center}
\end{figure}

We want to increase the homogeneity and reduce heterogeneity in
the resulting subnodes. In other words - we want subsets that are as
pure as possible w.r.t. class labels.

\end{frame}

\begin{frame}
\frametitle{Node Splitting}
\begin{block}{Splitting Categorical Attributes}
\begin{outline}
\1 Binary splits: use a set of possible values on one branch and its
complement on the other
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.6\textwidth]{figs/dt-split1.eps}
\end{center}
\end{figure}
\1 Multiway splits: use a separate branch for each possible value.
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.4\textwidth]{figs/dt-split2.eps}
\end{center}
\end{figure}
\end{outline}
\end{block}
\end{frame}

\begin{frame}
\frametitle{Node Splitting}
\begin{block}{Splitting Numeric Attributes}
\begin{outline}
\1 Binary splits: find a threshold and partition into values above and
below it
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.15\textwidth]{figs/dt-split3.eps}
\end{center}
\end{figure}
\1 Multiway splits: Discretize the values (statically as preprocessing or
dynamically) to form ordinal values
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.3\textwidth]{figs/dt-split4.eps}
\end{center}
\end{figure}
\end{outline}
\end{block}
\end{frame}

\section{Attribute Selection Measures}

\begin{frame}
\frametitle{Attribute Selection Measures}
We use impurity measures.	

Impurity can be quantified in several ways:
\begin{block}{Impurity Measures}
\begin{outline}
\1 Entropy (e.g., ID3 and C4.5)
\2 Information gain
\2 Gain ratio
\1 Gini index (e.g., CART, SLIQ, and SPRINT)
\end{outline}
\end{block}

In general, these measures are equivalent in most cases, but there are
specific cases when one can be advantageous over others.

\end{frame}

\begin{frame}
\frametitle{Entropy}

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.35\textwidth]{figs/dt-fig-entropy-new.ps}
\end{center}
\end{figure}

\begin{outline}
\1 $S$ is a sample of training examples
\1 $p_{\oplus}$ is the proportion of positive examples in $S$
\1 $p_{\ominus}$ is the proportion of negative examples in $S$
\1 Entropy measures the impurity of $S$
\[ Entropy(S) \equiv  - p_{\oplus} \log_{2} p_{\oplus} -  p_{\ominus} \log_{2}
p_{\ominus} \]
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Entropy}
$Entropy(S)$ = expected number of bits needed to encode class ($\oplus$ or
$\ominus$) of randomly drawn member of $S$ (under the optimal, shortest-length
code)
\bigskip

Why?

Information theory: optimal length code assigns $- \log_{2}p$ bits to
message having probability $p$.

\bigskip 

So, expected number of bits to encode $\oplus$ or $\ominus$ of random member
of $S$:
\[ p_{\oplus} (-\log_{2} p_{\oplus}) + p_{\ominus} (-\log_{2} p_{\ominus}) \]

\[ Entropy(S) \equiv  - p_{\oplus} \log_{2} p_{\oplus} -  p_{\ominus} \log_{2}
p_{\ominus} \]

\end{frame}

\begin{frame}
\frametitle{Entropy for more classes}
Calculate Entropy for more classes:
\begin{equation*}
E(S) = -\sum_{c} p(c) \log_2 p(c)
\end{equation*}
where $p(c)$ is the proportion of examples in $S$ belong to the class $c$.
\bigskip
\begin{outline}
\1 Minimum entropy is zero - achieved when all data points in the
node have the same class
\1 Maximum entropy is log(\#classes) - achieved when data points
in the node are equally distributed between the classes
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Example: Attribute Outlook}
\begin{outline}
\1 Outlook = sunny: 3 examples \textit{yes}, 2 examples \textit{no}
$E(Outlook=sunny)=?$
\1 Outlook = overcast: 4 examples \textit{yes}, 0 examples \textit{no}
$E(Outlook=overcast)=?$
\1 Outlook = rainy: 2 examples \textit{yes}, 3 examples \textit{no}
$E(Outlook=rainy)=?$
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Example: Attribute Outlook}
\begin{outline}
\1 Outlook = sunny: 3 examples \textit{yes}, 2 examples \textit{no}
$E(Outlook=sunny)=-\frac{3}{5}\log_2\frac{3}{5}-\frac{2}{5}\log_2\frac{2}{5}=0.971$
\1 Outlook = overcast: 4 examples \textit{yes}, 0 examples \textit{no}
$E(Outlook=overcast)=-1\log_2(1)-0\log_20 = 0$
\1 Outlook = rainy: 2 examples \textit{yes}, 3 examples \textit{no}
$E(Outlook=rainy)=-\frac{2}{5}\log_2\frac{2}{5}-\frac{3}{5}\log_2\frac{3}{5}=0.971$
\end{outline}
\end{frame}

\subsection{Information Gain}

\begin{frame}
\frametitle{Average Entropy / Information}
\begin{outline}
\1 \textbf{Problem}
\2 Entropy only computes the quality of a single (sub-)set of examples
\3 corresponds to a single value
\2 How can we compute the quality of the entire split?
\3 corresponds to an entire attribute
\1 \textbf{Solution}
\2 Compute the weighted average over all sets resulting from the split
\3 weighted by their size
\begin{equation*}
I(S,A)=\sum_i\frac{|S_i|}{|S|} E(S_i)
\end{equation*}
\1 \textbf{Example}
\2 Average entropy for attribute Outlook:
$I(Outlook)=\frac{5}{14}\times 0.971 + \frac{4}{14}\times 0 + \frac{5}{14}\times 0.971 = 0.693$
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Information Gain}
\begin{block}{Information Gain for Attribute A}
$Gain(S,A)$ = expected reduction in entropy due to sorting on $A$
\[ Gain(S,A) \equiv E(S) - I(S,A) = E(S)\ - \sum_{v \in Values(A)} \frac{|S_{v}|}{|S|}
E(S_{v}) \]
\end{block}
\begin{outline}
\1 The attribute that maximizes the difference is selected
\2 i.e., the attribute that reduces the unorderedness most!
\1 Note: 
\2 Maximizing information gain is equivalent to minimizing average
entropy
\3 $E(S)$ is constant for all attributes $A$
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Quiz}
Which attribute is best? 

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.8\textwidth]{figs/dt-s1.ps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Training Examples}
\begin{table}
\begin{small}
\begin{tabular}{c c c c c c c}
\hline
Day & Outlook & Temperature & Humidity & Wind & PlayTensis\\
\hline
D1 & Sunny & Hot & High & Weak & No \\
D2 & Sunny & Hot & High & Strong & No \\
D3 & Overcast & Hot & High & Weak & Yes \\
D4 & Rain & Mild & High & Weak & Yes \\
D5 & Rain & Cool & Normal & Weak & Yes \\
D6 & Rain & Cool & Normal & Strong & No \\
D7 & Overcast & Cool & Normal & Strong & Yes \\
D8 & Sunny & Mild & High & Weak & No \\
D9 & Sunny & Cool & Normal & Weak & Yes \\
D10 & Rain & Mild & Normal & Weak & Yes \\
D11 & Sunny & Mild & Normal & Strong & Yes \\
D12 & Overcast & Mild & High & Strong & Yes \\
D13 & Overcast & Hot & Normal & Weak & Yes \\
D14 & Rain & Mild & High & Strong & No \\  \hline
\end{tabular}
\end{small}
\caption{Training examples for the target concept $PlayTensis$}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Selecting the Next Attribute}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=1.0\textwidth]{figs/dt-inf.ps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
%\frametitle{Selecting the Next Attribute}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.75\textwidth]{figs/dt-t.ps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Limitations of Information Gain}
\begin{outline}
\1 Problematic: attributes with a large number of values
\2 extreme case: each example has its own value
\3 e.g. example ID; Day attribute in weather data
\1 Subsets are more likely to be pure if there is a large number of
different attribute values
\2 Information gain is biased towards choosing attributes with a
large number of values
\1 This may cause several problems:
\2 Overfitting
\3 selection of an attribute that is non-optimal for prediction
\2 Fragmentation
\3 data are fragmented into (too) many small sets
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Decision Tree for Day Attribute}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.75\textwidth]{figs/dt-day.eps}
\end{center}
\end{figure}
\begin{outline}
\1 Entropy of split
\begin{equation*}
I(\text{Day})=\frac{1}{14}\left(E([0,1]) + E([0,1]) +\cdots+E([0,1])\right)=0
\end{equation*}
\2 Information gain is maximal for Day (0.940 bits)
\end{outline}
\end{frame}

\subsection{Gain Ratio}

\begin{frame}
\frametitle{Gain Ratio}
\begin{outline}
\1 An extension to information gain
\1 Overcome to bias of information gain
\1 Normalize information gain using ``split information``
\begin{equation*}
\text{\textit{SplitInfo}}(D,A) = -\sum_{j=1}^{v}\frac{|D_j|}{|D|} \times \log_2\left(\frac{|D_j|}{|D|}\right)
\end{equation*}
where $D$ is the data set;  $A$ is an attribute; $v$ is the number of partitions corresponding to $v$ outcomes of a test on attribute $A$.
\1 Observation:
\2 Attributes with higher ``split information`` are less useful.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Gain Ratio}
\begin{block}{Gain Ratio}
The gain ratio is defined as
\begin{equation*}
GainRatio(D,A)=\frac{\text{\textit{Gain(D,A}})}{\text{\textit{SplitInfo}}(D,A)}
\end{equation*}
\end{block}
The attribute with the maximum gain ratio is selected as the splitting attribute.
\end{frame}

\begin{frame}
\frametitle{Quiz}
Compute the Gain Ratio of Outlook attribute?

$GainRatio(\text{Outlook})=?$
\end{frame}

\subsection{Gini Index}

\begin{frame}
\frametitle{Gini Index}
\begin{outline}
\1 Many alternative measures to Information Gain
\1 Most popular altermative: Gini index
\2 used in e.g., in CART (Classification And Regression Trees)
\2 impurity measure (instead of entropy)
\begin{equation*}
Gini(S)=1-\sum_i p_i^2
\end{equation*}
\2 average Gini index (instead of average entropy/information)
\begin{equation*}
Gini(S, A)=\sum_i \frac{|S_i|}{|S|} \times Gini(S_i)
\end{equation*}
\end{outline}
\end{frame}

\section{Decision Tree Implementation}

\begin{frame}
\frametitle{Implementation of Decision Trees}
See practice exercises on the course repository:

https://bitbucket.org/minhpqn/ail301-machine-learning

\end{frame}

\section{Tree Pruning}

\begin{frame}
\frametitle{Overfitting}
Consider error of hypothesis $h$ over
\begin{outline}
\1 training data: $error_{train}(h)$
\1 entire distribution $\cal{D}$ of data: $error_{\cal{D}}(h)$
\end{outline}

Hypothesis $h \in H$ {\bf overfits} training data if there is an alternative
hypothesis $h' \in H$ such that
\[  error_{train}(h) < error_{train}(h') \]
and
\[  error_{\cal{D}}(h) > error_{\cal{D}}(h') \]

\end{frame}

\begin{frame}
\frametitle{Overfitting in Decision Tree Learning}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=1.0\textwidth]{figs/dt-train-val.eps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Avoiding Overfitting}
How can we avoid overfitting?
\begin{outline}
\1 stop growing when data split not statistically significant
\1 grow full tree, then post-prune
\end{outline}

\bigskip

How to select ``best'' tree:
\begin{outline}
\1 Measure performance over training data 
\1 Measure performance over separate validation data set
\1 MDL: minimize\\
$size(tree) + size(misclassifications(tree))$
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Overfitting and Pruning}
\begin{outline}
\1 The smaller the complexity of a concept, the less danger that it
\2 A polynomial of degree $n$ can always fit $n+1$ points
overfits the data
\1 Thus, learning algorithms try to keep the learned concepts simple
\2 Note a ``perfect'' fit on the training data can always be found for a
decision tree! (except when data are contradictory)
\1[] \textcolor{blue}{Pre-Pruning}:
\2 stop growing a branch when information becomes unreliable
\1[] \textcolor{blue}{Post-Pruning}:
\2 grow a decision tree that correctly classifies all training data
\2 simplify it later by replacing some nodes with leafs
\1 Postpruning preferred in practice--prepruning can ``stop early''
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Prepruning}
\begin{outline}
\1 Based on statistical significance test
\2 Stop growing the tree when there is no statistically significant
association between any attribute and the class at a particular node
\1 Most popular test: \textit{chi-squared test}
\1 ID3 used chi-squared test in addition to information gain
\2 Only statistically significant attributes were allowed to be selected
by information gain procedure
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Post-prunning}
\begin{outline}
\1 Basic idea
\2 first grow a full tree to capture all possible attribute interactions
\2 later remove those that are due to chance
\end{outline}
\end{frame}

\end{document}

