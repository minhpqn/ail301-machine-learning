\documentclass[hyperref={pdfpagelabels=false},serif]{beamer}
\usepackage{lmodern}
\usepackage{xcolor}
\usepackage{subfigure}
\usetheme{Warsaw}
%\usetheme{Boadilla}
\setbeamertemplate{headline}{}
\usepackage{graphicx}
\usepackage{outlines}
\usepackage{courier}
\usepackage{amssymb, amsmath, amsfonts} 
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{fancyvrb}

\title[Multinomial Logistic Regression (MaxEnt)  \hspace{25mm} \insertframenumber/\inserttotalframenumber]{Multinomial Logistic Regression (MaxEnt)}
\author[Pham Quang Nhat Minh]{Pham Quang Nhat Minh}
\institute{FPT Technology Research Institute (FTRI)\\
minhpqn@fpt.edu.vn}
\date{\today} 
\begin{document}
\begin{frame}[plain]
\titlepage
\end{frame} 

% Hightlight current section
\AtBeginSection[]
{
\begin{frame}<beamer>
  \frametitle{Table of Contents}
  \tableofcontents[currentsection]
\end{frame}
}

\begin{frame}
\frametitle{Announcements}
\begin{outline}
\1 Presentation will contribute 15\% in the final grade
\1 Group 1 will present on Friday (please come to class on time: 9:10)
\1 Group 2 (Hung + Linh) will present on next week (PCA)
\1 Course project groups will have to report work progress every week.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Table of contents}
\tableofcontents
\end{frame}

\begin{frame}
\frametitle{Learning objectives}
\begin{outline}
\1 Explain model representation of Multinomial Logistic Regression (aka Maximum Entropy Models)
\1 Understand optimization algorithms used in MaxEnt models
\1 Use MaxEnt modeling toolkit for classification task
\1 Implement a text classification model with MaxEnt
\end{outline}
\end{frame}

\section{Introduction}

\begin{frame}
\frametitle{Introduction}
\begin{outline}
\1 Generative Models (Naive Bayes, HMM, Language Models,...)
\2 Model joint probability $P(x,y)$
\1 In NLP, Speech, IR (and Machine Learning generally), conditional or discriminative probabilistic models are used much
\2 They give high accuracy performance.
\2 They make it easy to incorporate lots of linguistically important features.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Joint vs. Conditional Models}
\begin{outline}
\1 We have some data $\{(d, c)\}$ of paired observations $d$ and hidden classes $c$.
\1 \textcolor{blue}{Joint (generative) models} place probabilities over both observed data and the hidden stuff (generate the observed data from hidden stuff) $P(c,d)$:
\2 All the classic StatNLP models:
\3 \textit{n}-gram models, Naive Bayes classifiers, hidden Markov models, probabilistic context-free grammars, IBM machine translation alignment models
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Joint vs. Conditional Models}
\begin{outline}
\1 \textcolor{blue}{Discriminative (conditional) models} take the data as given, and put a probability over hidden structure given the data:
\2 Logistic regression, conditional loglinear or maximum entropy models, conditional random fields
\2 Also, SVMs, (averaged) perceptron, etc. are discriminative classifiers (but not directly probabilistic)
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Conditional vs. Joint Likelihood}
\begin{outline}
\1 A joint model gives probabilities $P(d,c)$ and tries to maximize this joint likelihood.
\2 It turns out to be trivial to choose weights: just relative frequencies.
\1 A conditional model gives probabilities $P(c|d)$. It takes the data as given and models only the conditional probability of the class.
\2 We seek to maximize conditional likelihood.
\2 Harder to do
\2 More closely related to classification error.
\end{outline}
\end{frame}

\section{Model Representation}

\begin{frame}
\frametitle{Basic Ideas}
\begin{outline}
\1 We want to directly model the conditional probability $P(y|x)$ as a linear model
\2 We could not directly use $P(y=C|x)=\sum_{i=1}^N w_if_i = w \cdot f$
\3 It is not a legal probability
\1 How can we create a legal probability distribution?
\2 Use log-linear models
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Log-Linear Models in NLP}
We use exp function and normalize values to make a legal probability.
\begin{equation*}
p(c|x) = \frac{1}{Z} \text{exp} \sum_i w_if_i
\end{equation*}

\bigskip

Feature $f_i$ is a property of both the observation $x$ and the candidate output class $c$.

\begin{equation*}
p(c|x) = \frac{1}{Z} \text{exp}\left( \sum_i w_if_i(c,x)\right)
\end{equation*}

\end{frame}

\begin{frame}
\frametitle{Log-Linear Models in NLP}
Equation for computing the probability of $y$ being of class $c$ given the observation $x$ in MaxEnt model:

\begin{equation*}
p(c|x) = \frac{\text{exp} \left( \sum_{i=1}^N w_i f_i(c,x)  \right) }{\sum_{c' \in C} \text{exp} \left( \sum_{i=1}^N w_i f_i(c', x) \right)  }
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Model Representation}
\begin{outline}
\1 The multinomial logistic regression (MLR) model is defined as
\begin{equation*}
P(y=k|x;W_k) = \frac{1}{Z} \text{exp} (W_k^Tx)
\end{equation*}
where Z is the normalization term:
\begin{equation*}
Z = \sum_{k=1}^K P(y=k|x; W_k) = \sum_{k=1}^K \text{exp} (W_k^Tx)
\end{equation*}
\1 Parameters of the model
\begin{equation*}
\textbf{W}=
\begin{bmatrix}
(W_1)^T\\
\vdots\\
(W_K)^T\\
\end{bmatrix}
=
\begin{bmatrix}
W_{10} & W_{11} & \cdots & W_{1N}\\
\vdots & \vdots & \vdots & \vdots\\
W_{K0} & W_{K1} & \cdots & W_{KN}\\
\end{bmatrix}
\end{equation*}
\end{outline}
\end{frame}

\section{Features in Multinomial Logistic Regression}

\begin{frame}
\frametitle{Features}
\begin{outline}
\1 Features $f$ are pieces of evidences, which link aspects of what we observe $x$ with a class $y$ that we want to predict.
\1 A feature is a function $f: C \times D \rightarrow R$
\1 In NLP, we usually use \textbf{indicator functions}.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Example Features}
Assume we would like to classify a review into two categories: positive ($+$) and negative ($-$). There are some potential features ``great'', ``second-rate'', ``no''.

\begin{align*}
f_1(x,y) &=
  \begin{cases}
   1,& \text{if ``great''} \in x \text{ \& } y = +\\
    0,              & \text{otherwise}
  \end{cases}
  \\
f_2(x,y) &=
  \begin{cases}
   1,& \text{if ``second-rate''} \in x \text{ \& } y = -\\
    0,              & \text{otherwise}
  \end{cases}
  \\
 f_3(x,y) &=
  \begin{cases}
   1,& \text{if ``no''} \in x \text{ \& } y = -\\
    0,              & \text{otherwise}
  \end{cases}
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Example Features}
\begin{outline}
\1 Each of these features has a corresponding weight (can be positive or negative).
\1 Weights indicate strength of features as cues for classes
\2 $w_1(x)$: strength of feature ``great'' as a cue for the class $+$
\2 $w_2(x)$: strength of feature ``second-rate'' as a cue for the class $-$
\2 $w_3(x)$: strength of feature ``no'' as a cue for the class $-$
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Features}
\begin{outline}
\1 In NLP uses, usually a feature specifies 
\2[(1)] an indicator function -- a yes/no boolean matching function -- of properties of the input and 
\2[(2)] a particular class
$f_i(x, y) \equiv [\Phi(x) \land y = c_j]$            [Value is 0 or 1]
\2 They pick out a data subset and suggest a label for it.

\1 We will say that $\Phi(x)$ is a feature of the data $x$; 
\1 When, for each $c_j$, the conjunction $\Phi(x)  \land y = c_j$ is a feature of the data-class pair $(x, y)$

\end{outline}
\end{frame}

\section{Examples of MaxEnt Classifiers}

\begin{frame}
\frametitle{Example: Text Categorization}
\begin{outline}
\textcolor{red}{(Zhang and Oles 2001)}
\1 Features are presence of each word in a document and the document class (they do feature selection to use reliable indicator words)
\1 Tests on classic Reuters data set (and others)
\2 Naive Bayes: 77.0% F1
\2 Linear regression: 86.0%
\2 Logistic regression: 86.4%
\2 Support vector machine: 86.5%
\1 Paper emphasizes the importance of regularization (smoothing) for successful use of discriminative methods (not used in much early NLP/IR work)
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Other Maxent Classifier Examples}
\begin{outline}
\1 You can use a maxent classifier whenever you want to assign data points to one of a number of classes:
\2 Sentence boundary detection \textcolor{red}{(Mikheev 2000)}
\3 Is a period end of sentence or abbreviation?
\2 Sentiment analysis \textcolor{red}{(Pang and Lee 2002)}
\3 Features: Word unigrams, bigrams, POS counts, …
\2 PP attachment \textcolor{red}{(Ratnaparkhi 1998)}
\3 Attach to verb or noun? Features of head noun, preposition, etc.
\2 Parsing decisions in  general \textcolor{red}{(Ratnaparkhi 1997; Johnson et al. 1999, etc.)}
\end{outline}
\end{frame}

\section{Classification in MaxEnt}

\begin{frame}
\frametitle{Classification in MaxEnt}
We choose the class $c$ which maximize the probability $P(c|x)$.
\begin{align*}
\hat{c} = & \underset{c \in C} {\text{ argmax }} P(c|x) \\
= & \underset{c \in C} {\text{ argmax }} \frac{ \text{exp}\left( \sum_{i=1}^N w_i f_i(c,x) \right) }{ \sum_{c' \in C} \text{exp}\left( \sum_{i=1}^N w_i f_i(c',x) \right) }\\
= & \underset{c \in C} {\text{ argmax }} \text{exp} \sum_{i=1}^N w_i f_i(c,x)\\
= & \underset{c \in C} {\text{ argmax }} \sum_{i=1}^N w_i f_i(c,x)\\
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Classification in MaxEnt}
We choose the class $c$ which maximize the probability $P(c|x)$.
\begin{align*}
\hat{c} = & \underset{c \in C} {\text{ argmax }} \sum_{i=1}^N w_i f_i(c,x)\\
\end{align*}

Some notes:
\begin{outline}
\1 Computing actual probability is useful when the classifier is embedded in a larger system (such as in sequence labeling domain)
\2 Maximum Entropy Markov Models (MEMMs)
\1 In the equation, we only need to look at non-zero features.
\end{outline}
\end{frame}

\section{Learning MaxEnt Model}

\begin{frame}
\frametitle{Learning MaxEnt Model}
\begin{outline}
\1 We choose weights that make the classes of the training examples more likely.
\1 MaxEnt is trained with \textcolor{red}{conditional maximum likelihood estimation}.
\2 Choose the parameters $W$ that maximize the (log) probability of the $y$ labels in training data given the observations $x$.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Learning MaxEnt Model}
For each individual training example (observation) $x^{(j)}$ in the training set, the optimal weights are:
\begin{equation*}
\hat{w} = \underset{w} { \text{ argmax } } \log P(y^{(j)}|x^{(j)})
\end{equation*}

\bigskip

For the entire set of observations in the training set, the optimal weights would then be:
\begin{equation*}
\hat{w} = \underset{w} { \text{ argmax } } \sum_j \log P(y^{(j)}|x^{(j)})
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Learning MaxEnt Model}
The objective function $L$ that we will maximize:
\begin{align*}
L(w) = & \sum_j \log P(y^{(j)}|x^{(j)})\\
= & \sum_j \log \frac{ \text{exp} \left( \sum_{i=1}^N w_i f_i(y^{(j)}, x^{(j)})  \right) }{ \sum_{y' \in Y} \text{exp} \left( \sum_{i=1}^N w_i f_i(y'^{(j)}, x^{(j)})  \right) }\\
= & \sum_j \log \exp \left( \sum_{i=1}^N w_i f_i(y^{(j)}, x^{(j)}) \right)\\
&  - \sum_j \log \sum_{y' \in Y} \exp \left( \sum_{i=1}^N w_i f_i(y'^{(j)}, x^{(j)}) \right)
\end{align*}
\end{frame}

\begin{frame}
\frametitle{Learning MaxEnt Model}
Derivative for a given feature dimension $k$
\begin{equation*}
\frac{ \partial L(w)}{\partial w_k} = \sum_j f_k(y^{(j)}, x^{(j)}) - \sum_j \sum_{y' \in Y} P(y'|x^{(j)}) f_k(y'^{(j)},x^{(j)})
\end{equation*}
The equation has an interpretation as follows:

\begin{equation*}
\frac{ \partial L(w)}{\partial w_k} = \sum_j \text{Observed count}(f_k) - \text{Expected count}(f_k)
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Learning MaxEnt Model}
We can apply some optimization methods to minimize negative of $L(w)$
\begin{outline}
\1 Gradient descent (GD); Stochastic gradient descent (SGD)
\1 Iterative proportional fitting methods: Generalized Iterative Scaling (GIS) and Improved Iterative Scaling (IIS)
\1 Conjugate gradient (CG)
\1 Quasi-Newton methods -- limited memory variable metric (LMVM) methods, in particular, L-BFGS
\2 See: http://users.iems.northwestern.edu/~nocedal/PDFfiles/limited-memory.pdf
\2 ``Numerical Optimization: Understanding L-BFGS'': http://aria42.com/blog/2014/12/understanding-lbfgs
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Regularization}
\begin{outline}
\1 To avoid overfitting problem
\2 The weights for features fit too perfectly the training set
\2 Modeling noisy factors in the data. For instance, a feature only occur in one class.
\1 A \textcolor{red}{regularization term} is added to the objective function.
\begin{equation*}
\hat{w} = \underset{w} { \text{ argmax } } \sum_j \log P(y^{(j)}|x^{(j)}) - \alpha R(w)
\end{equation*}
where $R(w)$, the regularization term, is used to penalize large weights.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Regularization}
\textbf{L2 regularization} is a quadratic function of the weight values
\begin{equation*}
R(W) = ||W||_2^2 = \sum_{i=1}^N w_i^2
\end{equation*}

\bigskip

The L2 regularized objective function becomes:

\begin{equation*}
\hat{w} = \underset{w} { \text{ argmax } } \sum_j \log P(y^{(j)}|x^{(j)}) - \alpha \sum_{i=1}^N w_i^2
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Regularization}
\textbf{L1 regularization} is a linear function of the weight values
\begin{equation*}
R(W) = ||W||_1 = \sum_{i=1}^N |w_1|
\end{equation*}

\bigskip

The L1 regularized objective function becomes:

\begin{equation*}
\hat{w} = \underset{w} { \text{ argmax } } \sum_j \log P(y^{(j)}|x^{(j)}) - \alpha \sum_{i=1}^N |w_i|
\end{equation*}
\end{frame}

\begin{frame}
\frametitle{Regularization}
\begin{outline}
\1 Kinds of regularization come from statistics
\2 L1 regularization is called ``\textbf{the lasso}'' or \textbf{lasso regression}.
\2 L2 regularization is called ``\textbf{ridge regression}''
\1 L2 regularization is easier to optimize 
\1 L2 prefers weight vectors with many small weights, L1 prefers sparse solutions with some larger weights but many more weights set to zero (far fewer features).
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Feature Selection}
\begin{outline}
\1 \textbf{Feature Selection} is used to choose important features to keep and remove the rest.
\1 Features are ranked by how informative they are about the classification decision.
\1 Some common metrics for ranking features
\2 \textbf{information gain}, $\chi^2$ (chi-square), pointwise mutual information, GINI index.
\1 Feature selection is important for unregularized classifiers. 
\1 It is sometimes also used in regularized classifiers in applications where speed is critical.
\end{outline}
\end{frame}

\section{MaxEnt Modeling Tools}

\begin{frame}
\frametitle{Maximum Entropy Modeling Tools}
\begin{outline}
\1 Python
\2 \texttt{sklearn.linear\_model.LogisticRegression}: http://bit.ly/2bDrRAS
\2 \texttt{nltk.classify.maxent}: http://www.nltk.org/modules/nltk/classify/maxent.html
\1 C/C++
\2 ``A simple C++ library for maximum entropy classification'', by Tsuruoka: http://www.logos.ic.i.u-tokyo.ac.jp/~tsuruoka/maxent/
\2 ``Maximum Entropy Modeling Toolkit for Python and C++'', by Zhang: https://github.com/lzhang10/maxent
\2 ``Classias'': http://www.chokkan.org/software/classias/index.html.en
\1 Java
\2 Stanford Classifier: https://nlp.stanford.edu/software/classifier.html
\2 Apache OpenNLP: http://opennlp.apache.org/
\2 Implementation in Mallet: http://mallet.cs.umass.edu/classification.php
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Lecture Summary}
\begin{outline}
\1 Multinomial Logistic Regression (aka Maximum Entropy) is a discriminative conditional probabilistic classification method
\2 Directly model the conditional probability $P(y|x)$
\1 MaxEnt estimate parameters of the model using \textcolor{red}{conditional maximum likelihood estimation} method
\1 Regularization is important in MaxEnt to avoid overfitting.
\end{outline}
\end{frame}

\section{Practice Exercise: Using MaxEnt Tools for Classification}

\begin{frame}
\frametitle{Use scikit-learn for classification task}
\textbf{Exercise 1}: Iris flower classification 
\begin{outline}
\1 Data set: https://archive.ics.uci.edu/ml/datasets/iris
\2 Download data set into a folder such as iris
\1 We need to classify a flower into one of classes
\2 Iris Setosa
\2 Iris Versicolour
\2 Iris Virginica
\1 Attributes
\2[1.] sepal length in cm
\2[2.] sepal width in cm
\2[3.] petal length in cm
\2[4.] petal width in cm
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Exercise 1}
\begin{outline}
\1 Subtask 1: Load data from file into X, y (\texttt{numpy.array})
\1 Subtask 2: Split data into X\_train, y\_train, X\_test, y\_test with test size is 0.2
\1 Subtask 3: Fit the training data with Logistic Regression
\2 \texttt{sklearn.linear\_model.LogisticRegression}
\2 Default setting
\1 Subtask 4: Classify the test data
\1 Subtask 5: Report the classification result on the test data
\1 Subtask 6: Use multinomial and ``lbfgs'' solver when fitting the model on training set and report classification result on the test data
\2 Set multi\_class=`multinomial', solver=`lbfgs'
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Exercise 1}
\begin{outline}
\1 Subtask 7: Feature Scaling before fitting the model
\2 http://scikit-learn.org/stable/modules/preprocessing.html
\1 Subtask 8: Report classification result on test data when using feature scaling
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Exercise 2: Text classification}
\textbf{Sentiment classification}: Classify a sentence into positive or negative class.

+1 one of the greatest family-oriented , fantasy-adventure movies ever .

-1 enigma is well-made , but it's just too dry and too placid .

\bigskip

Download data \texttt{sentiment.txt} file (Movie review data by Bo Pang and Lillian Lee).

\end{frame}

\begin{frame}
\frametitle{Exercise 2: Text classification}
\begin{outline}
\1 Subtask 1: Load data into a list of sentences and a list of labels
\1 Subtask 2: Split data into train\_sentences, train\_labels, test\_sentences, test\_labels
\1 Subtask 3: Vectorize the training sentences using binary features and remove stop words
\1 Subtask 4: Transform training sentences, test sentences into vectors
\end{outline}
Then, do steps 3-8 as in the exercise 1.
\end{frame}

\begin{frame}
\frametitle{References}
\begin{outline}
\1 Chapter 7 ``Logistic Regression'', by Daniel Jurafsky.
\2 https://web.stanford.edu/~jurafsky/slp3/7.pdf
\1 Lecture slide: ``Maximum Entropy Classifiers'', by by Dan Jurafsky and Christopher Manning.
\2 https://web.stanford.edu/~jurafsky/NLPCourseraSlides.html
\1 Section 8.1, 8.2, 8.3 of the book "Machine Learning- A Probabilistic Perspect", by Kevin P. Murphy.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Next class}
\begin{outline}
\1 Presentation of Group 1 (first slot) about SVM/Kernel Methods
\1 Maximum Entropy Markov Models (MEMMs) and applications in Part-of-Speech (POS) Tagging
\end{outline}
\end{frame}

\end{document}

