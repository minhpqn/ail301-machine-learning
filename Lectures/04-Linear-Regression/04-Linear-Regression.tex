\documentclass[hyperref={pdfpagelabels=false},serif]{beamer}
\usepackage{lmodern}
\usepackage{xcolor}
\usepackage{subfigure}
\usetheme{Warsaw}
%\usetheme{Boadilla}
\setbeamertemplate{headline}{}
\usepackage{graphicx}
\usepackage{outlines}
\usepackage{courier}
\usepackage{amssymb, amsmath, amsfonts} 
\usepackage{mathtools}
\usepackage{hyperref}
\usepackage{fancyvrb}

\title[Linear Regression  \hspace{25mm} \insertframenumber/\inserttotalframenumber]{Linear Regression}
\author[Pham Quang Nhat Minh]{Pham Quang Nhat Minh}
\institute{FPT Technology Research Institute (FTRI)\\
minhpqn@fpt.edu.vn}
\date{\today} 
\begin{document}
\begin{frame}[plain]
\titlepage
\end{frame} 

% Hightlight current section
\AtBeginSection[]
{
\begin{frame}<beamer>
  \frametitle{Table of Contents}
  \tableofcontents[currentsection]
\end{frame}
}

\begin{frame}
\frametitle{Table of contents}
\tableofcontents
\end{frame}

\begin{frame}
\frametitle{Learning objectives}
\begin{outline}
\1 Explain model representation of linear regression
\1 Understand how to solve linear regression using normal equation methods
\1 Implement normal equation in python
\1 Use linear regression module in scikit-learn toolkit
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Readings}
\begin{outline}
\1 Chapter 3: "Linear Models for Regression", in the book "Pattern Recognition and Machine Learning", (Bishop)
\1 CS229 Lecture notes: Supervised Learning, Discriminative Algorithms. 
\2 http://cs229.stanford.edu/notes/cs229-notes1.pdf
\end{outline}
\end{frame}

\section{Introduction}

\begin{frame}
\frametitle{Example: Estimating Housing Prices}
\begin{outline}
\1 We would like to estimate housing prices in Hanoi based on
\2 $x_1$: Area (size) in $\text{m}^2$
\2 $x_2$: Number of bedrooms
\2 $x_3$: Distance to the city center in km.
\1 We have data of $1000$ houses in Hanoi
\1 How can we learn a function $y=f(\textbf{x})$ that map from a \textit{feature vector} $\textbf{x}=[x_1,x_2,x_3]$ to a real-valued output (housing price).
\1 To ease explanation and visualization, we just use one variable $x$: size in $\text{m}^2$.
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Training set of housing prices}
\begin{table}
\begin{tabular}{c | c}
Size in $\text{m}^2$ ($x$) & Price (in million VND) \\ 
\hline
30 & 630 \\
87 & 2190 \\
51 & 1292 \\
63 & 1588 \\
$\cdots$ & $\cdots$ \\
\end{tabular}
\end{table}
\begin{outline}
\1 Notations
\2 \textbf{m} = Number of training examples
\2 $\textbf{x}$ = ``input'' variables / features
\2 $\textbf{y}$ = ``output'' variables / ``target'' variable
\2 $(x,y)$ - one training example
\2 $(x^{(i)},y^{(i)})$ - $i^{th}$ training example
\end{outline}
\end{frame}

\section{Model Representation}

\begin{frame}
\frametitle{Model Representation}
% Write equation to represent linear regression models
Hypothesis function $h_{\theta}(\textbf{x})$ can be approximated by a linear function of $\textbf{x}$:
\begin{equation*}
h_{\theta}(\textbf{x}) = \theta_0 + \theta_1x_1 + \cdots + \theta_nx_n
\end{equation*}
where $\theta_0$, $\theta_1$,...,$\theta_n$ are parameters of the model. $\theta_0$ is the bias.

\bigskip

If we add a feature $x_0=1$, we can re-write the hypothesis function $h$ in the form:
\begin{equation*}
h_{\theta}(\textbf{x}) = \sum_{j=0}^{n} \theta_jx_j = \theta^T\textbf{x}
\end{equation*}
where $\theta=[\theta_0,\theta_1,\cdots,\theta_n]^T$ is the (column) parameter vector (weight vector); $\textbf{x}=[x_0,x_1,\cdots,x_n]^T$ is the input feature vector. 
\end{frame}

\begin{frame}
\frametitle{Linear Regression with One Variable (Univariate)}
% Write equation to represent linear regression models
Hypothesis function in the case of one variable:

\begin{equation*}
h_\theta(x) = \theta_0 + \theta_1x
\end{equation*}

\begin{figure}[!t]
\begin{center}
\includegraphics[width=0.5\textwidth]{figs/lr-train.eps}
\end{center}
\end{figure}

\end{frame}

\section{Loss Function}

\begin{frame}
\frametitle{Loss (Cost) Function}
Training examples: $(\textbf{x}^{(1)},\textbf{y}^{(1)}),\cdots,(\textbf{x}^{(m)},\textbf{y}^{(m)})$

\bigskip

To learn parameters of the model, we would like to minimize estimation error of the model on the training data.

\bigskip

We can can define the \textit{loss function} as:

\begin{equation*}
J(\theta) = \frac{1}{2m} \sum_{i=1}^m (h_\theta(\textbf{x}^{(i)}) - \textbf{y}^{(i)})^2
\end{equation*}

The above method is called \textit{Ordinary Least Squares} method.

\begin{equation*}
J(\theta) \rightarrow \text{min.}
\end{equation*}

\end{frame}

\begin{frame}
\frametitle{Optimization Problem}
\begin{table}
\begin{tabular}{l l l}
Hypothesis & & $h_\theta(\textbf{x})=\theta_0 + \theta_1x_1 + \cdots + \theta_nx_n$ \\\\
Parameters & & $\theta = \theta_0,\theta_1,\cdots,\theta_n$ \\\\
Cost (Loss) Function & & $J(\theta)=\frac{1}{2m} \sum_{i=1}^m (h_\theta(\textbf{x}^{(i)}) - \textbf{y}^{(i)})^2$\\\\
Goal & & $\underset{\theta} {\text{minimize }} J(\theta)$
\end{tabular}
\end{table}
\end{frame}

\section{Solving the Optimization Problem}

\begin{frame}
\frametitle{Normal Equation Method (1)}
We can solve the optimization problem by using normal equation method.

\bigskip

We represent all training examples in one matrix with dimension $m \times (n+1)$
\begin{equation*}
\textbf{X}=
\begin{bmatrix}
(\textbf{x}^{(1)})^T\\
\vdots\\
(\textbf{x}^{(m)})^T\\
\end{bmatrix}
=
\begin{bmatrix}
1 & x_{11} & \dots & x_{1n}\\
\vdots & \vdots & \vdots & \vdots\\
1 & x_{m1} & \dots & x_{mn}\\
\end{bmatrix}
\end{equation*}

\bigskip

$\textbf{y}$ is the column vector that contains output variables.

\[
\textbf{y}=
\begin{bmatrix}
y^{(1)}\\
\vdots\\
y^{(m)}
\end{bmatrix}
\]
\end{frame}

\begin{frame}
\frametitle{Normal Equation Method (2)}
Re-write loss function:
\begin{equation*}
J(\theta) = \frac{1}{2} (\textbf{X}\theta - \textbf{y}) (\textbf{X}\theta - \textbf{y}).
\end{equation*}

Gradient of $J(\theta)$ w.r.t $\theta$:
\begin{equation*}
\nabla  J(\theta) =  \frac{\partial J(\theta)}{\partial \theta} = \textbf{X}^T (\textbf{X}\theta - \textbf{y})
\end{equation*}

To minimize loss function $J$, we would like to find $\theta$ such that $\nabla  J(\theta) = 0$. The solution of the equation is:

\begin{equation*}
\hat{\theta} = (\textbf{X}^T\textbf{X})^{-1} \textbf{X}^T \textbf{y}.
\end{equation*}

The above equation is call \textbf{Normal Equation}.
\end{frame}

\begin{frame}
\frametitle{Normal Equation Method (3)}
\begin{outline}
\1 If the matrix $\textbf{X}^T\textbf{X}$ is inversable, we can find the only one solution $\theta$ by using Normal Equation.
\1 If the matrix $\textbf{X}^T\textbf{X}$ is not inversable, the equation has no solution or infinite solutions. In that case, we can still use \textit{pseudo inverse}.
\2 We can use function \texttt{numpy.linalg.pinv} in the library \texttt{numpy}.
\end{outline}
\end{frame}

\section{Example of Linear Regression}

\begin{frame}
\frametitle{Relation between Pressure and Boiling Point}
\begin{outline}
\1 In 1857, the Scottish physicist James D. Forbes studied the relation between
pressure and boiling point.
\1 Purpose of the work was to estimate altitude above sea level by measuring the boiling point of water.
\2 Barometers were fragile and difficult to transport at that time.
\1 Forbes collected data in Alps and Scotland.
\1 Forbes measured the attitude (in inches Hg), and boiling point (in Fahrenheit).
\end{outline}
\end{frame}

\begin{frame}
\frametitle{Pressure and Boiling Point}
\begin{table}
\begin{small}
\begin{tabular}{l | c | c}
& Boiling point ($^\text{o}$F) & Pressure\\
\hline
1 & 194.5 & 20.79 \\
2 & 194.3 & 20.79\\
3 & 197.9 & 22.40 \\
4 & 198.4 & 22.67 \\
5 & 199.4 & 23.15 \\
6 & 199.9 & 23.35 \\
7 & 200.9 & 23.89 \\
8 & 201.1 & 23.99 \\
9 & 201.4 & 24.02 \\
10 & 201.3 & 24.02 \\
11 & 203.6 & 25.14 \\
12 & 204.6 & 26.57 \\
13 & 209.5 & 28.49 \\
14 & 208.6 & 27.76 \\
15 & 210.7 & 29.04 \\
16 & 211.9 & 29.88 \\
17 & 212.2 & 30.06 \\
\hline
\end{tabular}
\end{small}
\end{table}
\end{frame}

\begin{frame}
\frametitle{Pressure and Boiling Point: Data Visualization}
\begin{figure}[!t]
\begin{center}
\includegraphics[width=1.0\textwidth]{figs/fb-1.eps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Pressure and Boiling Point}
The result of linear regression estimation is $h_\theta(x)=-81.06373 + 0.52289x$
\begin{figure}[!t]
\begin{center}
\includegraphics[width=.85\textwidth]{figs/fb-2.eps}
\end{center}
\end{figure}
\end{frame}

\begin{frame}
\frametitle{Practice Exercises}
\begin{outline}
\1 Implement parameter estimation of linear regression using Normal Equation
\1 Using scikit-learn's linear regression module for parameter estimation
\1 Evaluate the implementations on data sets (Forbes, Fuel)
\end{outline}
\end{frame}

\end{document}

