# Machine Learning Course (AIL301)

Lecturer: Pham Quang Nhat Minh

## Lecture outline

### Lecture 3: Decision Trees

#### Learning Objectives

- Explain decision-tree induction algorithms
- Implement a decision tree classifier
- Try decision-tree algorithms in some off-the-self machine learning toolkits

#### Readings

**Required readings**

- Chapter 1: "Decision Trees", A Course in Machine Learning, by Hal Daume III.
- Chapter 3: "Decision Tree Learning", Machine Learning, by Tom Mitchell.
- Chapter 6, section 6.3; "Classification by Decision Tree Induction", Han, J., & Kamber, M. (2006). Data Mining: Concepts and Techniques. Soft Computing (Vol. 54). https://doi.org/10.1007/978-3-642-19721-5.

**Optional readings**

- Chapter 6: "Trees and rules", Data Mining: Practical Machine Learning Tools and Techniques (2016), by Witten, Ian H.
- CS 391L: Machine Learning: Decision Tree Learning, by Raymond J. Mooney, University of Texas at Austin

### Lecture 4: Linear Regression

#### Learning Objectives

- Understand model representation of linear regression
- Understand how to solve linear regression using normal equation methods
- Implement normal equation in python using python
- Use linear regression module in scikit-learn toolkit

#### Readings

**Required readings**

- Chapter 3: "Linear Models for Regression", in the book "Pattern Recognition and Machine Learning", (Bishop)
- [CS229 Lecture notes: Supervised Learning, Discriminative Algorithms](http://cs229.stanford.edu/notes/cs229-notes1.pdf)

**Optional readings**

- [Bài 3: Linear Regression](http://machinelearningcoban.com/2016/12/28/linearregression/), on the website [http://machinelearningcoban.com](http://machinelearningcoban.com) (in Vietnamese)

### Lecture 5: Gradient Descent Algorithms

#### Learning Objectives

- Explain gradient descent algorithms
- Use Gradient Descent algorithm to solve linear regression problems
- Implement gradient descent algorithms

#### Readings

**Required readings**

- [Bài 7: Gradient Descent (phần 1/2)](http://machinelearningcoban.com/2017/01/12/gradientdescent/), by Vũ Hữu Tiệp
- [Bài 8: Gradient Descent (phần 2/2)](http://machinelearningcoban.com/2017/01/16/gradientdescent2/), by Vũ Hữu Tiệp

**Optional readings**

- [An overview of gradient descent optimization algorithms](http://sebastianruder.com/optimizing-gradient-descent/), by Sebastian Ruder.
- [An Interactive Tutorial on Numerical Optimization](http://www.benfrederickson.com/numerical-optimization/), by Ben Frederickson
- Section 4.3 "Gradient-Based Optimization" in the book "Deep Learning" [http://www.deeplearningbook.org/](http://www.deeplearningbook.org/).
- Section 5.9 "Stochastic Gradient Descent", in the book "Deep Learning" [http://www.deeplearningbook.org/](http://www.deeplearningbook.org/).

### Lecture 6: Logistic Regression

#### Learning Objectives

- Explain the model representation of logistic regression
- Know how to estimate paramaters of the logistic regression model
- Explain regularization methods to avoid overfitting
- Understand how to use ```sklearn.linear_model.LogisticRegression``` class for classification problem.

#### Readings

**Required readings**

- [Bài 10: Logistic Regression](http://machinelearningcoban.com/2017/01/27/logisticregression/), by Vũ Hữu Tiệp
- [CS229 Lecture notes: Supervised Learning, Discriminative Algorithms](http://cs229.stanford.edu/notes/cs229-notes1.pdf)

**Optional readings**

- [Cramer, Jan Salomon. “The origins of logistic regression.” (2002).](https://papers.tinbergen.nl/02119.pdf)

### Lecture 7: Hidden Markov Models

See the lecture slides on the course website.

### Lecture 8: Multinomial Logistic Regression (Maximum Entropy Models)

#### Learning Objectives

- Explain model representation of Maximum Entropy Models
- Understand optimization algorithms used in MaxEnt models
- Use MaxEnt modeling toolkit for classification task
- Implement a text classification model with MaxEnt

#### Readings

**Required readings**

- [Maximum Entropy Classifiers](http://spark-public.s3.amazonaws.com/nlp/slides/Maximum_Entropy_Classifiers_v2.pdf)
- [Maxent Models and Discriminative Estimation](http://spark-public.s3.amazonaws.com/nlp/slides/AdvancedMaxent.pdf)
- [Chapter 7 "Logistic Regression"](https://web.stanford.edu/~jurafsky/slp3/7.pdf), by Daniel Jurafsky.

**Optional readings**

- Section 8.1, 8.2, 8.3 of the book "Machine Learning- A Probabilistic Perspect", by Kevin P. Murphy.
- [Principle of Maximum Entropy](https://www.slideshare.net/JiawangLiu/maxent)

### Lecture 9: Maximum Entropy Markov Models

#### Learning Objectives

- Explain model representation of MEMMs
- Understand decoding in MEMMs
- Explain differences between MEMMs and HMMs
- Know how to use MEMM tool-kits for sequence labeling.

#### Readings

- [Section 10.5, 10.6, Chapter 10 "Part-of-speech tagging"](https://web.stanford.edu/~jurafsky/slp3/10.pdf), by Daniel Jurafsky.
- Lecture notes of [Michael Collins](http://www.cs.columbia.edu/~mcollins/)

## References

- [CS 229 Machine Learning Course Materials](http://cs229.stanford.edu/materials.html)
- [Bài giảng, tài liệu](https://sites.google.com/a/wru.vn/cse445spring2016/lecture-materials), ĐHTL
- [http://www.cs.cmu.edu/~tom/mlbook-chapter-slides.html](http://www.cs.cmu.edu/~tom/mlbook-chapter-slides.html)
- Website: [http://machinelearningcoban.com](http://machinelearningcoban.com) (in Vietnamese), you can find list of blog posts here [http://machinelearningcoban.com/archive/](http://machinelearningcoban.com/archive/)
- Website: [https://ml-book-vn.khanhxnguyen.com/](https://ml-book-vn.khanhxnguyen.com/)
- [Python Numpy Tutorial](http://cs231n.github.io/python-numpy-tutorial/)
