# Presentation topics for AIL301 - Machine Learning (Summer 2017)

Lecturer: Pham Quang Nhat Minh

Topics:

1. SVM/Kernel Methods
2. Dimension Reduction, PCA
3. Ensemble Learning Methods
   + Random Forest
   + Bagging
   + AdaBoost

## Requirements

- All members in each group MUST present.
- Grades will be given for members separetely.
- Groups should be able to show their sound understanding about the assigned topics.
- Groups NEED to prepare their own slides!
- In the presentation, please show how to apply algorithms using off-the-shelf Machine Learning toolkits
and apply on the real data sets.

### Grading criteria

- How a presentor understand the topic she/he presents.
- The presentation is understandable and clear.
- The presentation slides are well-prepared.
- Practical examples in the presentation is useful.

## Groups and Topics

### Group 1

- Members: Hoa, Thúy, Huyền
- Topic: SVM/Kernel Methods
- Presentation date: July 21, 2017 (Fri)

**Recommended Readings**

- Chapter 11 "Kernel Methods" in the book "A Course in Machine Learning", by Hal Daume III.
- [Bài 19: Support Vector Machine](http://machinelearningcoban.com/2017/04/09/smv/)
- [Bài 20: Soft Margin Support Vector Machine](http://machinelearningcoban.com/2017/04/13/softmarginsmv/)
- [Bài 21: Kernel Support Vector Machine](http://machinelearningcoban.com/2017/04/22/kernelsmv/)
- [Bài 22: Multi-class Support Vector Machine](http://machinelearningcoban.com/2017/04/28/multiclasssmv/)
- [Kernel Methods and Support Vector Machines](https://pdfs.semanticscholar.org/70e6/b231e00fd94eb379aa81a1d5cfc95b949d1e.pdf)
- [Support Vector Machines and Kernel Methods](https://www.cs.cmu.edu/~ggordon/SVMs/new-svms-and-kernels.pdf), by Geoff Gordon.
- [Kernel Methods and Support Vector Machines](http://www.jaist.ac.jp/~bao/VIASM-SML/Lecture/L3-Kernel%20methods%20and%20SVM.pdf)

**Other Good References**

- [SVM Tutorial](https://www.svm-tutorial.com/)
- [Support Vector Machine (and Statistical Learning Theory) Tutorial](http://www.cs.columbia.edu/~kathy/cs4701/documents/jason_svm_tutorial.pdf), by Jason Weston.
- [An Idiot’s guide to Support vector machines (SVMs)](http://www.svms.org/tutorials/Berwick2003.pdf)
- Chapter 6 "Kernel Methods" in the book "Pattern Recognition and Machine Learning", by Bishop (Advanced)

**Softwares**

- [sklearn.svm.SVC](http://scikit-learn.org/stable/modules/generated/sklearn.svm.SVC.html)
- [LIBSVM – A Library for Support Vector Machines](https://www.csie.ntu.edu.tw/~cjlin/libsvm/)

### Group 2

- Members: Hùng, Linh
- Topic: Dimension Reduction, PCA
- Presentation date: July 26, 2017

**Recommended Readings**

- Lecture "Principal Component Analysis File", by Dr. Le Hong Phuong.
- [Bài 27: Principal Component Analysis (phần 1/2)](https://machinelearningcoban.com/2017/06/15/pca/)
- [Bài 28: Principal Component Analysis (phần 2/2)](https://machinelearningcoban.com/2017/06/21/pca2/)

### Group 3

- Members: Giang, Thịnh, Huyền
- Topic: Ensemble Learning Methods (Random Forest, Bagging, AdaBoost)
- Presentation date: August 4, 2017

**Recommended Readings**

- [Tutorial on Ensemble Learning](http://infochim.u-strasbg.fr/new/CS3_2010/Tutorial/Ensemble/EnsembleModeling.pdf)
- [Trees, Bagging, Random Forests and Boosting](http://jessica2.msri.org/attachments/10778/10778-boost.pdf)
- [1.11. Ensemble methods](http://scikit-learn.org/stable/modules/ensemble.html)
- Section 16.2 "Adaptive basis function models" in Murphy's textbook
- [L. Breiman, “Random Forests”, Machine Learning, 45(1), 5-32, 2001.](https://link.springer.com/article/10.1023/A:1010933404324)
- [AdaBoost Tutorial](http://mccormickml.com/2013/12/13/adaboost-tutorial/)
- [A Tutorial on Boosting](http://www.di.unipi.it/~cardillo/AA0304/fabio/boosting.pdf)
- [http://www-math.mit.edu/~rothvoss/18.304.3PM/Presentations/1-Eric-Boosting304FinalRpdf.pdf](http://www-math.mit.edu/~rothvoss/18.304.3PM/Presentations/1-Eric-Boosting304FinalRpdf.pdf)
- [Bagging and Random Forest Ensemble Algorithms for Machine Learning](http://machinelearningmastery.com/bagging-and-random-forest-ensemble-algorithms-for-machine-learning/)
- [Bagging and Boosting](https://www.cs.rit.edu/~rlaz/prec20092/slides/Bagging_and_Boosting.pdf)


